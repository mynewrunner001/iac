
module "app" {
  source = "./module/app/"
  namespace_name = "ox1d5"
  replicas = 2
  service_port = 5000
}
