
variable "namespace_name" {}

variable "replicas" {
  default = 3
}

variable "service_port" {
  default = 5000
}

# Create a namespace
resource "kubernetes_namespace" "name" {
  metadata {
    name = var.namespace_name
  }
}

# Create an App deployment
resource "kubernetes_deployment" "app" {
  metadata {
    name      = "app"
    namespace = kubernetes_namespace.name.metadata.0.name
  }
  spec {
    replicas = var.replicas

    selector {
      match_labels = {
        app = "app"
      }
    }

    template {
      metadata {
        labels = {
          app = "app"
        }
      }

      spec {
        container {
          name = "app"
          image = "ox1d0/flask_app:lastest"
          port {
            container_port = 5000
          }
        }
      }
    }
  }
}

# Create an App service
resource "kubernetes_service" "app" {
  metadata {
    name      = "app"
    namespace = kubernetes_namespace.name.metadata.0.name
  }

  spec {
    selector = {
      app = kubernetes_deployment.app.spec.0.template.0.metadata.0.labels.app
    }

    port {
      port        = var.service_port
      target_port = 5000
    }
    
  }
}
#end
/*resource "helm_release" "nginx_ingress" {
  name             = "ingress"
  repository       = "https://kubernetes.github.io/ingress-nginx"
  chart            = "ingress-nginx"
  version          = "4.5.2"
  namespace        = kubernetes_namespace.name.metadata.0.name
  create_namespace = true
  wait             = true

  set {
    name  = "controller.replicaCount"
    value = 2
  }
  
  set {
    name  = "controller.service.type"
    value = "NodePort"
  }
}
*/